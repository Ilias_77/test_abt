import Vuex from 'vuex'
import persons from './persons'


export default new Vuex.Store({
  modules: { persons }
})