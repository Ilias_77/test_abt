export default {
  state: {
    persons: JSON.parse(localStorage.getItem("persons") || "[]"),
  },
  mutations: {
    createPerson(state, person) {
      state.persons.push(person);
      localStorage.setItem("persons", JSON.stringify(state.persons));
    },
    updatePerson(state, person) {
      const persons = state.persons.slice();
      const idx = persons.findIndex((t) => t.id === person.id);
      const task = persons[idx];

      persons[idx] = person;
      state.persons = persons;
      localStorage.setItem("persons", JSON.stringify(state.persons));
    },
    deletePerson(state, id) {
      const idx = state.persons.findIndex((t) => t.id === id);
      state.persons.splice(idx, 1);
      localStorage.setItem("persons", JSON.stringify(state.persons));
    },
  },
  actions: {
    createPerson({ commit }, person) {
      commit("createPerson", person);
    },
    updatePerson({ commit }, person) {
      commit("updatePerson", person);
    },
    deletePerson({ commit }, person) {
      commit("deletePerson", person);
    },
  },
  getters: {
    persons: (s) => s.persons,
    personById: (s) => (id) => s.persons.find((t) => t.id === id),
  },
};
