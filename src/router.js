import { createRouter, createWebHistory } from "vue-router";

const routes = [
  {
    name: "Home",
    path: "/",
    component: () => import("./pages/Home.vue"),
  },
  {
    name: "Create",
    path: "/create",
    component: () => import("./pages/Create.vue"),
  },
  {
    name: "Edit",
    path: "/edit/:id",
    component: () => import("./pages/Edit.vue"),
  },
];

export default createRouter({
  history: createWebHistory(),
  routes,
});
